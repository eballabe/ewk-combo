import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

def get_pts(filename):
    spt0 = filename.split('WZ')[1]
    spt1 = spt0.split('_')
    pt1 = spt1[1]
    pt2 = spt1[2].split('.')[0]
    pair = [pt1, pt2]
    return pair

def read_num(num_str):
    num_f = num_str.replace('p', '.')
    num_f = float(num_f)
    return num_f

path = 'xls_files/'
# files
f0L = pd.read_excel(path+'WZ-0l_EXT.xlsx')
f1L = pd.read_excel(path+'WZ-1l_EXT.xlsx')
f2L = pd.read_excel(path+'WZ-2l2j_off-shell_aod_EXT.xlsx')
f3L_off16 = pd.read_excel(path+'WZ-3l_off-shell_susy16_EXT.xlsx')
f3L_off2 = pd.read_excel(path+'WZ-3l_off-shell_susy2_EXT.xlsx')
f3L_on = pd.read_excel(path+'WZ-3l_on-shell_EXT.xlsx')

f_orig_all_a = pd.read_excel(path+'MC16a_ATLMCPROD-7179.xlsx')
f_orig_all_d = pd.read_excel(path+'MC16d_ATLMCPROD-7179.xlsx')
f_orig_all_e = pd.read_excel(path+'MC16e_ATLMCPROD-7179.xlsx')

f_orig_2L2J_a = pd.read_excel(path+'C1N2_via_WZ_2L2J_MC16a.xlsx')
f_orig_2L2J_d = pd.read_excel(path+'C1N2_via_WZ_2L2J_MC16d.xlsx')
f_orig_3L_a = pd.read_excel(path+'C1N2_via_WZ_3L_MC16a.xlsx')
f_orig_3L_d = pd.read_excel(path+'C1N2_via_WZ_3L_MC16d.xlsx')

# f2L_add = pd.read_excel(path+'submit_signal_Wino_offshellWZ_2L7MET75_MC16x.xlsx')
f0L_1L_add = pd.read_excel(path+'WZ_0L_1L_unknown.xlsx')
f2L_add = pd.read_excel(path+'WZ_2L_unknown.xlsx')
f3L_on_add = pd.read_excel(path+'WZ_3L_onshell_unknown.xlsx')
f3L_off_add = pd.read_excel(path+'WZ_3L_offshell_unknown.xlsx')

f_all_missing = pd.read_excel(path+'WZ_missing_pts.xlsx')

# collect pts
xpoints_0L = []
ypoints_0L = []
for file1 in f0L['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_0L.append(read_num(pair_pts[0]))
    ypoints_0L.append(read_num(pair_pts[1]))

xpoints_1L = []
ypoints_1L = []
for file1 in f1L['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_1L.append(read_num(pair_pts[0]))
    ypoints_1L.append(read_num(pair_pts[1]))

xpoints_2L = []
ypoints_2L = []
for file1 in f2L['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_2L.append(read_num(pair_pts[0]))
    ypoints_2L.append(read_num(pair_pts[1]))

xpoints_3L_off16 = []
ypoints_3L_off16 = []
for file1 in f3L_off16['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_3L_off16.append(read_num(pair_pts[0]))
    ypoints_3L_off16.append(read_num(pair_pts[1]))

xpoints_3L_off2 = []
ypoints_3L_off2 = []
for file1 in f3L_off2['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_3L_off2.append(read_num(pair_pts[0]))
    ypoints_3L_off2.append(read_num(pair_pts[1]))

xpoints_3L_on = []
ypoints_3L_on = []
for file1 in f3L_on['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_3L_on.append(read_num(pair_pts[0]))
    ypoints_3L_on.append(read_num(pair_pts[1]))

xpoints_orig_all = []
ypoints_orig_all = []
for file1 in f_orig_all_a['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_all.append(read_num(pair_pts[0]))
        ypoints_orig_all.append(read_num(pair_pts[1]))
for file1 in f_orig_all_d['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_all.append(read_num(pair_pts[0]))
        ypoints_orig_all.append(read_num(pair_pts[1]))
for file1 in f_orig_all_e['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_all.append(read_num(pair_pts[0]))
        ypoints_orig_all.append(read_num(pair_pts[1]))

xpoints_orig_2L2J = []
ypoints_orig_2L2J = []
for file1 in f_orig_2L2J_a['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_2L2J.append(read_num(pair_pts[0]))
        ypoints_orig_2L2J.append(read_num(pair_pts[1]))
for file1 in f_orig_2L2J_d['JobOptions']:
    print(file1, type(file1))
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_2L2J.append(read_num(pair_pts[0]))
        ypoints_orig_2L2J.append(read_num(pair_pts[1]))

xpoints_orig_3L = []
ypoints_orig_3L = []
for file1 in f_orig_3L_a['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_3L.append(read_num(pair_pts[0]))
        ypoints_orig_3L.append(read_num(pair_pts[1]))
for file1 in f_orig_3L_d['JobOptions']:
    if isinstance(file1,str) and 'WZ' in file1:
        pair_pts = get_pts(file1)
        xpoints_orig_3L.append(read_num(pair_pts[0]))
        ypoints_orig_3L.append(read_num(pair_pts[1]))


xpoints_0L_1L_add = []
ypoints_0L_1L_add = []
for file1 in f0L_1L_add['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_0L_1L_add.append(read_num(pair_pts[0]))
    ypoints_0L_1L_add.append(read_num(pair_pts[1]))


xpoints_2L_add = []
ypoints_2L_add = []
for file1 in f2L_add['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_2L_add.append(read_num(pair_pts[0]))
    ypoints_2L_add.append(read_num(pair_pts[1]))

xpoints_3L_on_add = []
ypoints_3L_on_add = []
for file1 in f3L_on_add['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_3L_on_add.append(read_num(pair_pts[0]))
    ypoints_3L_on_add.append(read_num(pair_pts[1]))

xpoints_3L_off_add = []
ypoints_3L_off_add = []
for file1 in f3L_off_add['JobOptions']:
    pair_pts = get_pts(file1)
    xpoints_3L_off_add.append(read_num(pair_pts[0]))
    ypoints_3L_off_add.append(read_num(pair_pts[1]))

xpoints_miss_all = []
ypoints_miss_all = []
for pair1 in f_all_missing['Point to be required']:
    pair_pts = pair1.split('_')
    xpoints_miss_all.append(read_num(pair_pts[0]))
    ypoints_miss_all.append(read_num(pair_pts[1]))


# plot & save pdf
fig, ax = plt.subplots()
plt.grid(True)

# # missing
# ax.plot(xpoints_miss_all, ypoints_miss_all, 'o', markersize=17, color='slategray', label='WZ all missing',alpha=0.1)

# # original request
# ax.plot(xpoints_orig_2L2J,ypoints_orig_2L2J,'s', color = 'black',  markersize=10, label='WZ 2L2J orig',alpha=0.9) 
# ax.plot(xpoints_orig_3L,ypoints_orig_3L,'>', color = 'cyan',  markersize=10, label='WZ 3L orig',alpha=0.9) 
# ax.plot(xpoints_orig_all,ypoints_orig_all,'o', color = 'magenta',  markersize=10, label='WZ 0L/1L orig',alpha=0.9) 
# # extended
# ax.plot(xpoints_0L,ypoints_0L,'o', color = 'green', markersize=8,  label='WZ 0L EXT',alpha=0.9) 
# ax.plot(xpoints_1L,ypoints_1L,'>',color='yellow', label='WZ 1L EXT',alpha=0.9) 
# ax.plot(xpoints_2L,ypoints_2L,'s',color='red',label='WZ 2L2J EXT',alpha=0.9)
# ax.plot(xpoints_3L_off2,ypoints_3L_off2,'x',color='blue',label='WZ 3L off-shell SUSY2 EXT',alpha=0.9)
# ax.plot(xpoints_3L_off16,ypoints_3L_off16,'+',color='lime',label='WZ 3L off-shell SUSY6 EXT',alpha=0.9)
# ax.plot(xpoints_3L_on,ypoints_3L_on,'*',color='orange',label='WZ 3L on-shell EXT',alpha=0.9) 


xpoints_2L = xpoints_2L + xpoints_2L_add
ypoints_2L = ypoints_2L + ypoints_2L_add
xpoints_3L = xpoints_3L_on + xpoints_3L_on_add
ypoints_3L = ypoints_3L_on + ypoints_3L_on_add


ax.plot(xpoints_orig_all,ypoints_orig_all,'o', color='lightblue', markersize=11, label='WZ 0L/1L (ATLMCPROD-7179)',alpha=0.9)
ax.plot(xpoints_0L,ypoints_0L,'o', color='deepskyblue', markersize=11,  label='WZ 0L (EXP COMB-2)',alpha=0.9) 
ax.plot(xpoints_0L_1L_add,ypoints_0L_1L_add,'o', markersize=11, color='deeppink',label='WZ 0L/1L (EXP COMB-2)',alpha=0.1)

ax.plot(xpoints_1L,ypoints_1L,'o',markersize=6,color='darkgreen',label='WZ 1L (EXP COMB-2)',alpha=0.9) 

ax.plot(xpoints_orig_2L2J,ypoints_orig_2L2J,'x', color = 'red',  label='WZ 2L (ATLMCPROD-5963)',alpha=0.9) 
ax.plot(xpoints_2L,ypoints_2L,'x',color='brown',label='WZ 2L (EXP COMB-2)',alpha=0.9)
# ax.plot(xpoints_2L_add,ypoints_2L_add,'x',color='blueviolet',label='WZ 2L2J EXT ADD',alpha=0.9)

ax.plot(xpoints_orig_3L,ypoints_orig_3L,'_', color = 'yellow',  markersize=10, label='WZ 3L (ATLMCPROD-5963)',alpha=0.9) 
ax.plot(xpoints_3L_on,ypoints_3L_on,'_',color='orange',label='WZ 3L on-shell (EXP COMB-2)',alpha=0.9) 
# ax.plot(xpoints_3L_on_add,ypoints_3L_on_add,'_', color='lime', label='WZ 3L on-shell EXT ADD',alpha=0.9)
# ax.plot(xpoints_3L_off_add,ypoints_3L_off_add,'4', markersize=12, color='maroon', label='WZ 3L off-shell EXT ADD',alpha=0.9)
# ax.plot(xpoints_3L_off2,ypoints_3L_off2,'x',color='blue',label='WZ 3L off-shell SUSY2 EXT',alpha=0.9)
# ax.plot(xpoints_3L_off16,ypoints_3L_off16,'+',color='lime',label='WZ 3L off-shell SUSY6 EXT',alpha=0.9)


ax.xaxis.set_ticks(np.arange(100, 1350, 100))
ax.yaxis.set_ticks(np.arange(0, 550, 50))
# ax.legend(loc='upper left', fontsize='small')
# ax.legend()
ax.legend(loc='upper left', fontsize='medium')


plt.title(r"$\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm}$ Wino $WZ$ signal grids")
plt.xlabel(r"$m(\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm})$ [GeV]")
plt.ylabel(r"$m(\tilde{\chi}_{1}^{0})$ [GeV]")

plt.savefig('common_pts_WZ_full_v7_1.pdf', dpi=2000, format='pdf', bbox_inches='tight')

plt.show(block=False)
plt.pause(0.001)
input("Press [Enter] to continue.")