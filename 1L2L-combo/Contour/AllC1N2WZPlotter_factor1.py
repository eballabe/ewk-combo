#!/usr/bin/env python

import ROOT, sys, os
import mycontourPlotter
import argparse

parser = argparse.ArgumentParser(description = 'Plot contours')

parser.add_argument("-a","--analyses",dest="analyses",help="Comma separated list of analyses to show in plot", default="Compr,3LoffS,3LonS,AllHad,2L2J,1L,WZCombo,1L2L")
parser.add_argument("-i", "--input", dest="inputType", help="recalc or original", default="recalc")
parser.add_argument("-o","--optimizer", dest="opt", default="scipy", help="scipy or minuit")
parser.add_argument("-b","--backend", dest="bk", default="jax", help="pytorch, numpy, jax or tensorflow")
parser.add_argument("-e","--extras",dest="Extras",default="",help="CLsExp, CLsObs or xsUL")
parser.add_argument("-dM","--deltaM",action='store_true',help="Flag to plot dM contour")
parser.add_argument("-ts","--theorySysts",action='store_true',help="Flag to plot +-1 sigma theory band", default=True)
#parser.add_argument("-logdM","--logdeltaM",action='store_true',help="Flag to plot with log dM interpolation")
parser.add_argument("-nsb","--noSigmaBand",action='store_true',help="Flag to remove one sigma band")
parser.add_argument("-asb","--allSigmaBands",action='store_true',help="Flag to plot sigma bands besides the combination")
parser.add_argument("-l","--labelPlot", action='store_true',help="Flag to add labels to each curve in the plot")
parser.add_argument("-xmin","--xmin",dest="xmin", help="Start of x axis")
parser.add_argument("-xmax","--xmax",dest="xmax", help="End of x axis")
parser.add_argument("-ymin","--ymin",dest="ymin", help="Start of y axis")
parser.add_argument("-ymax","--ymax",dest="ymax", help="End of y axis")
args = parser.parse_args()

# Currently not plotting theory systs
drawTheorySysts = args.theorySysts
drawIndividualFilled = True

# Get list of analyses to plot
analysisList = []
argAnalyses = args.analyses.split(',')
for a in argAnalyses:
    if a not in ['WZCombo', 'Compr', '3LoffS', '3LonS', 'AllHad', '2L2J', '1L','1L2L']:
        raise ValueError('Analysis',a,'not recognised.')

for a in ['Compr', '3LoffS', '3LonS', 'AllHad', '2L2J', '1L', 'WZCombo','1L2L']:
    if a in argAnalyses:
        analysisList.append(a)
                
# Get name of output file

bk_opt_str = ""
if args.inputType=='recalc':
    bk_opt_str = args.bk+"_"+args.opt

dM_str = ""
if args.deltaM:
    dM_str = "_DM"
#if args.logdeltaM:
#    dM_str = "_logdeltaM"
#if args.deltaM and args.logdeltaM:
#    print("Can't have both dM and logdM.")
#    sys.exit(1)

Extras = args.Extras

extra_str = ""
if args.noSigmaBand:
    extra_str += "_noSigmaBand"
if args.allSigmaBands:
    extra_str += "_allSigmaBands"

plottername = 'C1N2WZ_Paper_'+Extras+"_"+args.inputType+"_"+bk_opt_str+dM_str+extra_str

# Dictionaries of plotting stuff
rootFiles = {
    'Compr'   : "CLs_Compr/graphs_Nominal_"+bk_opt_str+".json_DM.root",
    '3LoffS'  : "CLs_3LoffS/graphs_Nominal_"+bk_opt_str+".json_DM.root",
    '3LonS'   : "CLs_3LonS/graphs_Nominal_"+bk_opt_str+".json"+dM_str+".root",
    'AllHad'  : "CLs_AllHad/graphs_Nominal_"+bk_opt_str+".json"+dM_str+".root",
    '2L2J'    : "CLs_2L2J/graphs_Nominal_"+bk_opt_str+".json"+dM_str+".root",
    '1L'      : "CLs_1L/graphs_Nominal_"+bk_opt_str+".json.root",
    'WZCombo' : "CLs_WZCombo/graphs_Nominal_"+bk_opt_str+".json_LogDM.root",
    '1L2L'    : "CLs_1L2L/outputGraphs_factor1.root",
}

lineColors = {
    '3LonS'     : ROOT.kOrange-3,
    '3LoffS'    : ROOT.kOrange-6,
    'AllHad'    : ROOT.kGreen-2,
    'Compr'     : ROOT.kMagenta-4,
    '1L'        : ROOT.kMagenta-1,
    '2L2J'      : ROOT.kAzure-6,
    'WZCombo'   : ROOT.kRed+2,
    '1L2L'   : ROOT.kRed,
}
#lineColors = {
#    '3LonS'     : ROOT.kAzure-6,
#    'AllHad'    : ROOT.kGreen-2,
#    'Compr'     : ROOT.kMagenta-1,
#    '1L'        : ROOT.kGray+2,
#    '2L2J'      : ROOT.kOrange+8,
#    '3LoffS'    : ROOT.kMagenta-3,
#    'WZCombo'   : ROOT.kRed+2,
#}

bandColors = {
    '3LonS'     : ROOT.kAzure-4,
    'AllHad'    : ROOT.kGreen-8,
    'Compr'     : ROOT.kMagenta-8,
    '1L'        : ROOT.kGray,
    '2L2J'      : ROOT.kOrange+6,
    '3LoffS'    : ROOT.kMagenta-7,
    'WZCombo'   : ROOT.TColor.GetColor("#ffd700"),
    '1L2L'      : ROOT.TColor.GetColor("#ffd600"),
}

#if args.deltaM or args.logdeltaM:
#       labelPos = {
#       'Compr'         : (460,520),
#         '3LoffS'      : (300, 120),
#         '3LonS'         : (110, 70),
#         'AllHad'      : (150, 300),
#         '2L2J'    : (630, 700),
#         '1L'      : (730, 800),
#       }
#
#else:
#       labelPos = {
#       '1L'            : (330, 180),
#       '2L0JWave1'     : (90, 130),
#       '2L0JWave2'     : (160, 16),
#       '2L0Jbest'      : (90, 100),
#       'AllHad'        : (610, 100),
#       }
        
labelPos = {
    'Compr'     : (460,520),
    '3LoffS'    : (300, 120),
    '3LonS'     : (110, 70),
    'AllHad'    : (150, 300),
    '2L2J'      : (630, 700),
    '1L'        : (730, 800),
    '1L2L'        : (700, 800)
}

labels =  {
    'WZCombo'   : 'Combination',
    'Compr'     : "#splitline{2L Compressed}{arXiv:1911.12606}",
    '3LoffS'    : "#splitline{3L off-shell}{arXiv:2106.01676}",
    '3LonS'     : "#splitline{3L on-shell}{arXiv:2106.01676}",
    'AllHad'    : "#splitline{All Hadronic}{arXiv:2108.07586}",
    '2L2J'      : "#splitline{2L2J}{arXiv:2204.13072}",
    '1L2L'      : "1L&2L2J Combined",
    '1L'        : "#splitline{1L}{arXiv:2310.08171}"  #scale[0.75]{ATLAS-CONF-2022-059}}"
}

# Set-up plot
plot = mycontourPlotter.mycontourPlotter(plottername, 800,600) # 1600, 1200)
lumi = "139"
#region = "1L+2L0J+AllHad Combination"
plot.processLabel = "Wino #tilde{#chi}^{0}_{2}#tilde{#chi}^{#pm}_{1} #rightarrow WZ #tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}"
plot.processLabel += ""
plot.processLabel_position = (0.15, 0.93)
plot.limitCaveatLabel = "All limits at 95% CL"
plot.limitCaveatLabel_position = (0.82, 0.85)
plot.ATLASLabel = "ATLAS"
plot.ATLASLabel_position = (0.55, 0.95)
plot.PrelimLabel = "Internal"
#plot.PrelimLabel = "Preliminary"
#plot.PrelimLabel = ""
plot.PrelimLabel_position = (0.55, 0.91)
plot.lumiLabel = "#sqrt{s}=13 TeV, "+lumi+" fb^{-1}"
plot.lumiLabel_position = (0.75, 0.93)
plot.anaLabel = ""
plot.anaLabel_position = (0.58,0.46)

# Axis ranges
xmin, xmax, ymin, ymax = 120, 1150, 0, 500
if args.deltaM: # or args.logdeltaM:
        xmin, xmax, ymin, ymax = 120, 500, 0, 200
if args.xmin:
        xmin = int(args.xmin)
if args.xmax:
        xmax = int(args.xmax)
if args.ymin:
        ymin = int(args.ymin)
if args.ymax:
        ymax = int(args.ymax)
plot.drawAxes( [xmin,ymin,xmax,ymax] )

name_OneSigmaBand = "Band_1s_0"
name_Observed = "Obs_0"
name_Expected = "Exp_0"
name_ObsUp = "Obs_0_Up"
name_ObsDo = "Obs_0_Down"

ExclText = ROOT.TLatex()
ExclText.SetTextSize(0.028)

def changeCoord1D(graph, name):
    if args.deltaM:
        if "_DM" in name:
            return graph
        elif "_LogDM" in name:
            return changeCoord1D_logDMtoDM( graph )
        else:
            return changeCoord1D_DM( graph )
    else:
        if "_DM" in name:
            return changeCoord1D_DM( graph )
        elif "_LogDM" in name:
            return changeCoord1D_DM( changeCoord1D_logDMtoDM( graph ) )
        else:
            return graph

def changeCoord2D(graph, name):
    if args.deltaM:
        if "_DM" in name:
            return graph
        elif "_LogDM" in name:
            return changeCoord2D_logDMtoDM( graph )
        else:
            return changeCoord2D_DM( graph )
    else:
        if "_DM" in name:
            return changeCoord2D_DM( graph )
        elif "_LogDM" in name:
            return changeCoord2D_DM( changeCoord1D_logDMtoDM( graph ) )
        else:
            return graph

def changeCoord1D_logDMtoDM(graph):
    x, y, n = graph.GetX(), graph.GetY(), graph.GetN()
    for i in range(n):
            newy = round(10**y[i],1)
            graph.SetPoint(i, x[i], newy)
    return graph

def changeCoord2D_logDMtoDM(graph):
    x, y, z, n = graph.GetX(), graph.GetY(), graph.GetZ(), graph.GetN()
    for i in range(n):
            newy = round(10**y[i],1)
            graph.SetPoint(i, x[i], newy, z[i])
    return graph

def changeCoord1D_DM(graph):
    # same transformation for DM->Norm. and Norm->DM
    x,y,n = graph.GetX(), graph.GetY(), graph.GetN()
    for i in range(n) :
        # change y coordinate
        newy = x[i] - y[i]
        graph.SetPoint(i, x[i], newy)
    return graph

def printGraph(graph):
    x, y, n = graph.GetX(), graph.GetY(), graph.GetN()
    for i in range(n):
        print(i, x[i], y[i])

def changeCoord2D_DM(graph):
    # same transformation for DM->Norm. and Norm->DM
    x,y,n = graph.GetX(), graph.GetY(), graph.GetZ(), graph.GetN()
    for i in range(n) :
        # change y coordinate
        newy = x[i] - y[i]
        graph.SetPoint(i, x[i], newy, z[i])
    return graph

# Plot each analysis
for a in analysisList:
    f = ROOT.TFile(rootFiles[a])
    print(rootFiles[a])
    lineColor = lineColors[a]
    bandColor = bandColors[a]
    curve_b = changeCoord1D(f.Get(name_OneSigmaBand), rootFiles[a])
    curve_o = changeCoord1D(f.Get(name_Observed), rootFiles[a])
    curve_e = changeCoord1D(f.Get(name_Expected), rootFiles[a])
    if curve_b and not args.noSigmaBand: 
        alpha=0.3
        if a=='WZCombo':
            alpha=0.5
            plot.drawOneSigmaBand(curve_b, color=bandColor, alpha=alpha)
        elif args.allSigmaBands:
            alpha=0.5
            plot.drawOneSigmaBand(curve_b, color=bandColor, alpha=alpha)
    if curve_e: 
        plot.drawExpected(curve_e, color=lineColor)
    if curve_o:
        #printGraph(curve_o)
        ## Do some tweaks..
        if a=='3LonS' and args.deltaM :
            curve_o.SetPoint(0, 100.0, 80.0)
            curve_o.SetPoint(1, 170.0, 79.0)
            curve_o.SetPoint(curve_o.GetN(), 100.0, 210.0)
        if a=='2L2J' and args.deltaM :
            curve_o.SetPoint(curve_o.GetN(), 100.0, 210.0)
        if a=='1L2L':
            curve_o.SetPoint(curve_o.GetN(), 40.0, 0.0)
        # Draw the observed
        if a!='WZCombo' and drawIndividualFilled==True : 
            plot.drawObserved(curve_o, color=lineColor, drawOption="LF")
        else :
            plot.drawObserved(curve_o, color=lineColor)
    if drawTheorySysts and a=='WZCombo':
        plot.drawTheoryUncertaintyCurve( changeCoord1D(f.Get(name_ObsUp), rootFiles[a]), color=lineColor )
        plot.drawTheoryUncertaintyCurve( changeCoord1D(f.Get(name_ObsDo), rootFiles[a]), color=lineColor )

    ExclText.SetTextColorAlpha(lineColor,0.7)
    if args.labelPlot and a in labelPos:
        ExclText.DrawLatex(labelPos[a][0],labelPosdM[a][1],a)

# Make legend
#standardLeg = (0.80,0.65,1,0.8)
#specialLeg = (0.80,0.20,1,0.60)
standardLeg = (0.80,0.5,1,0.56)
specialLeg = (0.80,0.15,1,0.48)

bandcolors = []
linecolors = []
labelList = []
bandcol_combo = bandColors['WZCombo']
linecol_combo = lineColors['WZCombo']

legendOrder = ['WZCombo', 'Compr', '3LoffS', '3LonS', 'AllHad', '2L2J', '1L', '1L2L']
for a in legendOrder:
    if a not in analysisList:
        continue
    if a=='WZCombo': continue
    bandcolors.append( bandColors[a])
    linecolors.append( lineColors[a])
    labelList.append( labels[a] )

#f = ROOT.TFile("CLs_1L2L/outputGraphs_factor1.root")
#plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")

## Combination entry
plot.drawLegendStandard( shape=(0.80,0.62,1,0.76), 
                         dummycurve = curve_o,
                         lineColor = linecol_combo,
                         fillColor = bandcol_combo
                       )
## Extra text
ExtraText = ROOT.TLatex()
ExtraText.SetTextSize( 0.028 )
ExtraText.SetTextColorAlpha( ROOT.kBlack, 1.0 )
ExtraText.DrawLatexNDC( 0.808, 0.77,"Combination" )
ExtraText.DrawLatexNDC( 0.808, 0.57,"Individual Analyses" )

## Standard entry
plot.drawLegendStandard( shape = standardLeg, 
                         dummycurve = curve_o,
                         lineColor = ROOT.kBlack,
                         fillColor = None
                       )
## Individual entries
plot.drawLegendSpecial( shape = specialLeg,
                        dummycurve = curve_o,
                        linecolors = linecolors,
                        bandcolors = None, #bandcolors,
                        labels = labelList 
                      )


## Draw Lines
if not args.deltaM:
    plot.drawLine( coordinates = [120,29,591,500], color=ROOT.kGray+1, label = "#it{m}(#tilde{#chi}^{0}_{2}) = #it{m}(#tilde{#chi}^{0}_{1}) + #it{m}(#it{Z})", style = 7, labelLocation=[365,300], angle = 58 )
    plot.drawLine( coordinates = [120,120,500,500], color=ROOT.kGray+1, label = "#it{m}(#tilde{#chi}^{0}_{2}) = #it{m}(#tilde{#chi}^{0}_{1})", style = 7, labelLocation=[265,300], angle = 58 )
else:
    plot.drawLine(  coordinates = [120,91,500,91], color=ROOT.kGray+1, label = "#Deltam(#tilde{#chi}^{#pm}_{1}, #tilde{#chi}^{0}_{1}) = #it{m}(#it{Z})", style = 7, labelLocation=[390,97], angle = 0 )

## Axis Labels
plot.setXAxisLabel( "m(#tilde{#chi}_{1}^{#pm}) [GeV]" )

yLabel = "m(#tilde{#chi}_{1}^{0}) [GeV]"
if args.deltaM: # or args.logdeltaM:
    yLabel="#Deltam(#tilde{#chi}_{1}^{#pm}, #tilde{#chi}_{1}^{0})"
plot.setYAxisLabel( yLabel  )

plot.decorateCanvas( )

fCombi = ROOT.TFile(rootFiles["WZCombo"])
if Extras=="CLsExp": plot.writePlotCLs( changeLogCoord2D(fCombi.Get("CLsexp_gr")), "Exp", xmax=xmax, ymax=ymax)
elif Extras=="CLsObs": plot.writePlotCLs( changeLogCoord2D(fCombi.Get("CLs_gr")), "Obs",xmax=xmax,ymax=ymax )
elif Extras=="xsUL": plot.writePlotCLs( changeLogCoord2D(fCombi.Get("upperLimit_gr")), "xsUL" ,xmax=xmax,ymax=ymax)
else:
    plot.writePlot( )
