import os

def construct_path_mass(mass_pt_str, combo_type):
    path = os.path.join('../../queue_condor_runs', combo_type, mass_pt_str)
    return path

def rerurn_one(br, mass_pt_str, combo_type):
    # path where mass point folder is
    mass_path = construct_path_mass(mass_pt_str, combo_type)
    job_path = os.path.join(mass_path, 'condor_jobs', 'job_br'+str(br)+'.sub')
    submit_file = job_path
    print('point', mass_pt_str, 'br =', str(br))
    print('condor_submit '+ job_path)
    os.system('condor_submit '+ submit_file)

    

def rerun_missing(mass_pt_str, combo_type):
    # search for the missing points and rerun them
    mass_path = construct_path_mass(mass_pt_str, combo_type)
        # path where to search for the txt files
    txt_path = os.path.join(mass_path, 'br_outputs')
    files_br = []
    with os.scandir(txt_path) as listOfEntries:
        for entry in listOfEntries:
            # all .txt files:
            if entry.is_file() and '.txt' in entry.name:
                files_br.append(entry.name)
    missing_br = [] 
    name_pt1 = mass_pt_str+'_br'
    for br_i in range(0, 101):
        if (name_pt1+str(br_i)+'.txt') not in files_br:
            print('result for br =',br_i,'is away')
            # rerurn_one(br_i, mass_pt_str, combo_type)
            missing_br.append(br_i)
    print(len(missing_br), 'files are missing')
