import numpy as np
import json
# from CLs_functions import CLs_0l_combined
# from CLs_functions import CLs_3l_combined
from CLs_functions import CLs_full_combination
from CLs_functions import CLs_my_full_combination
import time

def construct_empty_dict(N_pts):
    pts_dict = dict()
    names = ['CLs', 'CLexp_0', 'CLexp_plus', 'CLexp_minus']
    for name in names:
        pts_dict[name] = dict()
    pts_dict['N'] = N_pts
    return pts_dict

def add_CLS(dict_pts, out_CLs, br):
    dict_pts['CLs'][str(br)] = float(out_CLs['CLs_obs'])
    dict_pts['CLexp_0'][str(br)] = float(out_CLs['CLs_exp_0sigma'])
    dict_pts['CLexp_plus'][str(br)] = float(out_CLs['CLs_exp_+1sigma'])
    dict_pts['CLexp_minus'][str(br)] = float(out_CLs['CLs_exp_-1sigma'])



mass_pt = [200, 50]
N = 1
#N = 51
time_st00 = time.time()
pts_dict = construct_empty_dict(N)
br_values = np.linspace(0, 100, N)
k = 0
time_st0 = time.time()
for br in br_values:
    k = k + 1
    print('measurement', k, ', br =', br)
    time_st1 = time.time()
    out1 = CLs_full_combination(br, mass_pt)
    time_st2 = time.time()
    out2 = CLs_my_full_combination(br, mass_pt)
    time_st2_add = time.time()
    print("out1:", out1)
    print("out2:", out2)
    # br is written in the range  of  0 - 1.0 in file
    #add_CLS(pts_dict, out, round(br/100, 2))
time_st3 = time.time()
# write to json file
#jsonname = 'Plots/400_100_combo03_TIMETEST.json'
#with open(jsonname, 'w', encoding='utf-8') as fh:
#    fh.write(json.dumps(pts_dict, ensure_ascii=False))
time_st4 = time.time()
print('all time:', time_st4 - time_st00, 'sec')
print('1 cycle:', time_st3 - time_st0, 'sec')
print('CLs_full_combination(br, mass_pt):', time_st2 - time_st1, 'sec')
print('CLs_my_full_combination(br, mass_pt):', time_st2_add - time_st2, 'sec')
print('initialize:', time_st0 - time_st00, 'sec')
#print('write json', time_st4 - time_st3, 'sec')
