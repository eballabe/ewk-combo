import numpy as np
import json
from CLs_functions import CLs_0l_combined
from CLs_functions import CLs_3l_combined
from CLs_functions import CLs_full_combination

# def construct_name(mass_pt):
#     pass

# file_WZ = "C1N2WZ_0L_600_100_combined_NormalMeasurement_model.json"
# file_Wh = "C1N2Wh_0L_600_100_combined_NormalMeasurement_model.json"

def construct_empty_dict(N_pts):
    pts_dict = dict()
    names = ['CLs', 'CLexp_0', 'CLexp_plus', 'CLexp_minus']
    for name in names:
        pts_dict[name] = dict()
    pts_dict['N'] = N_pts
    return pts_dict

def add_CLS(dict_pts, out_CLs, br):
    dict_pts['CLs'][str(br)] = float(out_CLs['CLs_obs'])
    dict_pts['CLexp_0'][str(br)] = float(out_CLs['CLs_exp_0sigma'])
    dict_pts['CLexp_plus'][str(br)] = float(out_CLs['CLs_exp_+1sigma'])
    dict_pts['CLexp_minus'][str(br)] = float(out_CLs['CLs_exp_-1sigma'])



mass_pt = [400, 0]
N = 101
#N = 51
pts_dict = construct_empty_dict(N)
br_values = np.linspace(0, 100, N)
k = 0
for br in br_values:
    k = k + 1
    print('measurement', k, ', br =', br)
    out = CLs_3l_combined(br, mass_pt)
    print("out:", out)
    # br is written in the range  of  0 - 1.0 in file
    add_CLS(pts_dict, out, round(br/100, 2))

# write to json file
jsonname = 'Plots/400_0_3l_new.json'
with open(jsonname, 'w', encoding='utf-8') as fh:
    fh.write(json.dumps(pts_dict, ensure_ascii=False))
