import json
import matplotlib.pyplot as plt
import numpy as np





def extract_data(file):
    data = json.load(open(file))
    
    N = data.get('N')
    br_values = np.linspace(0.0,1.0,N)
    
    CLs = np.zeros((N))
    CLexp_0 = np.zeros((N))
    CLexp_plus = np.zeros((N))
    CLexp_minus = np.zeros((N))
    
    for i in range(N):
        label = str(br_values[i])
        CLs[i] = data.get('CLs').get(label)
        CLexp_0[i] = data.get('CLexp_0').get(label)
        CLexp_plus[i] = data.get('CLexp_plus').get(label)
        CLexp_minus[i] = data.get('CLexp_minus').get(label)
        continue
    
    return CLs, CLexp_0,CLexp_minus,CLexp_plus, br_values






mass_point = '200_50' #string of form: 'm1_m2' , i.e.,  '200_50'
file = mass_point + '_0l.json' 
CLs, CLexp_0,CLexp_plus,CLexp_minus, br_values = extract_data(file) 

plt.plot(100*br_values, CLs, linestyle = '--', color = 'k', label = 'observed')
plt.plot(100*br_values, CLexp_0, linestyle = '--' , color = 'b', label = 'Expected 0 sig')
plt.fill_between(100*br_values,CLexp_plus, CLexp_minus, color = 'b', alpha = 0.15, label = 'Expectation band')
plt.xlim(0,100) 
plt.yscale('log')
plt.ylabel('CLs ')
plt.title('0l - ' + mass_point +' mass point')
plt.xlabel('Branching Ratio WZ %')
#plt.axhline(0.05, color = 'g',label = 'CLs = 0.05', linestyle = '--' )
plt.legend() 
plt.savefig('Plot_b_example.png',dpi = 400)