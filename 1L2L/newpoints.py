import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np


xpoints = []
ypoints = []

xpoints += [200, 200]
ypoints += [  0,  50]

xpoints += [250, 250, 250]
ypoints += [  0,  50, 100]

xpoints += [300, 300, 300, 300]
ypoints += [  0,  50, 100, 150]

xpoints += [350, 350, 350, 350, 350]
ypoints += [  0,  50, 100, 150, 200]

xpoints += [400, 400, 400, 400, 400, 400]
ypoints += [  0,  50, 100, 150, 200, 250]

xpoints += [450, 450, 450, 450, 450, 450, 450]
ypoints += [  0,  50, 100, 150, 200, 250, 300]

xpoints += [500, 500, 500, 500, 500, 500, 500, 500]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350]

xpoints += [550, 550, 550, 550, 550, 550, 550, 550, 550]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400]

xpoints += [600, 600, 600, 600, 600, 600, 600, 600, 600, 600]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450]

xpoints += [650, 650, 650, 650, 650, 650, 650, 650, 650, 650, 650]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints += [700, 700, 700, 700, 700, 700, 700, 700, 700, 700, 700]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints += [750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints += [800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints += [900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900]
ypoints += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints += [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000]
ypoints += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

xpoints += [1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100]
ypoints += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

xpoints += [1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200]
ypoints += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

# plot & save pdf
fig, ax = plt.subplots()
plt.grid(True)

ax.plot(xpoints, ypoints, 'o', markersize=6, color='darkgreen', label='1L/2L $WZ/Wh/WW$ mediated decays', alpha=0.9)

ax.xaxis.set_ticks(np.arange(100, 1350, 100))
ax.yaxis.set_ticks(np.arange(0, 550, 50))

ax.legend(loc='upper left', fontsize='medium')

plt.title(r"$\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm}$ Wino-Bino and Higgsino-Bino signal grids")
plt.xlabel(r"$m(\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm})$ [GeV]")
plt.ylabel(r"$m(\tilde{\chi}_{1}^{0})$ [GeV]")

plt.savefig('NewPoints.pdf', dpi=2000, format='pdf', bbox_inches='tight')

plt.show(block=False)
plt.pause(0.001)
input("Press [Enter] to continue.")
