import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np


xpoints_200 = []
ypoints_200 = []

xpoints_200 += [200, 200]
ypoints_200 += [  0,  50]

xpoints_250 = []
ypoints_250 = []

xpoints_250 += [250, 250, 250]
ypoints_250 += [  0,  50, 100]

xpoints_300 = []
ypoints_300 = []

xpoints_300 += [300, 300, 300, 300]
ypoints_300 += [  0,  50, 100, 150]

xpoints_350 = []
ypoints_350 = []

xpoints_350 += [350, 350, 350, 350, 350]
ypoints_350 += [  0,  50, 100, 150, 200]

xpoints_400 = []
ypoints_400 = []

xpoints_400 += [400, 400, 400, 400, 400, 400]
ypoints_400 += [  0,  50, 100, 150, 200, 250]

xpoints_450 = []
ypoints_450 = []

xpoints_450 += [450, 450, 450, 450, 450, 450, 450]
ypoints_450 += [  0,  50, 100, 150, 200, 250, 300]

xpoints_500 = []
ypoints_500 = []

xpoints_500 += [500, 500, 500, 500, 500, 500, 500, 500]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350]

xpoints_500 += [550, 550, 550, 550, 550, 550, 550, 550, 550]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400]

xpoints_500 += [600, 600, 600, 600, 600, 600, 600, 600, 600, 600]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450]

xpoints_500 += [650, 650, 650, 650, 650, 650, 650, 650, 650, 650, 650]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints_500 += [700, 700, 700, 700, 700, 700, 700, 700, 700, 700, 700]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints_500 += [750, 750, 750, 750, 750, 750, 750, 750, 750, 750, 750]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints_500 += [800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints_500 += [900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900]
ypoints_500 += [  0,  50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

xpoints_500 += [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000]
ypoints_500 += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

xpoints_500 += [1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100, 1100]
ypoints_500 += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

xpoints_500 += [1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200]
ypoints_500 += [   0,   50,  100,  150,  200,  250,  300,  350,  400,  450,  500]

# plot & save pdf
fig, ax = plt.subplots()
plt.grid(True)

ax.plot(xpoints_200, ypoints_200, 'o', markersize=6, color='brown', label='160k', alpha=0.9)
ax.plot(xpoints_250, ypoints_250, 'o', markersize=6, color='red', label='80k', alpha=0.9)
ax.plot(xpoints_300, ypoints_300, 'o', markersize=6, color='orange', label='40k', alpha=0.9)
ax.plot(xpoints_350, ypoints_350, 'o', markersize=6, color='green', label='30k', alpha=0.9)
ax.plot(xpoints_400, ypoints_400, 'o', markersize=6, color='darkgreen', label='20k', alpha=0.9)
ax.plot(xpoints_450, ypoints_450, 'o', markersize=6, color='blue', label='20k', alpha=0.9)
ax.plot(xpoints_500, ypoints_500, 'o', markersize=6, color='purple', label='10k', alpha=0.9)

ax.xaxis.set_ticks(np.arange(100, 1350, 100))
ax.yaxis.set_ticks(np.arange(0, 550, 50))

ax.legend(loc='upper left', fontsize='medium')

plt.title(r"$\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm}$ Wino-Bino and Higgsino-Bino signal grids")
plt.xlabel(r"$m(\tilde{\chi}_{2}^{0}/\tilde{\chi}_{1}^{\pm})$ [GeV]")
plt.ylabel(r"$m(\tilde{\chi}_{1}^{0})$ [GeV]")

plt.savefig('NewPointsColors.pdf', dpi=2000, format='pdf', bbox_inches='tight')

plt.show(block=False)
plt.pause(0.001)
input("Press [Enter] to continue.")
